import * as Discord from 'discord.js';

export class EpsilonVoice {
    //public static client: Discord.Client = new Discord.Client();
    public static settings = {
        allowInterjections: true,
        leaveAfterFinishing: false,
        forceReloadOnInterjection: true,
        volume: 1.0
    };

    public static info: {
        channel: Discord.VoiceChannel | null,
        connection: Discord.VoiceConnection | null,
        lastChannel: Discord.VoiceChannel | null,
        speaking: boolean
    } = {
        channel: null,
        connection: null,
        lastChannel: null,
        speaking: true
    };

    public static joinChannel(target: Discord.VoiceChannel): Promise<void> {
        return new Promise((resolve, reject) => {
            if((EpsilonVoice.settings.forceReloadOnInterjection && EpsilonVoice.info.speaking) || EpsilonVoice.info.channel != target) {
                if(EpsilonVoice.info.channel != null)
                    (<Discord.VoiceChannel>(EpsilonVoice.info.channel!)).leave();
            }
            target.join().then(conn => {
                EpsilonVoice.info.channel = target;
                EpsilonVoice.info.lastChannel = target;
                EpsilonVoice.info.connection = conn;
                return resolve();
            }).catch(err => {
                console.log(err);
                return reject();
            });
        });
    }

    public static leaveChannel(): boolean {
        if(EpsilonVoice.info.channel == null) return false;
        EpsilonVoice.info.channel.leave();
        EpsilonVoice.info.channel = null;
        return true;
    };

    public static playStream(stream: any, channel?: Discord.VoiceChannel): Promise<void> {
        const promise: Promise<void> = new Promise<void>((resolve, reject) => {
            if(!channel && !EpsilonVoice.info.channel && !EpsilonVoice.info.lastChannel) reject();
            EpsilonVoice.joinChannel(channel || EpsilonVoice.info.channel || EpsilonVoice.info.lastChannel!).then(() => {
                EpsilonVoice.info.connection!.playStream(stream).on('end', () => {
                    if(EpsilonVoice.settings.leaveAfterFinishing) EpsilonVoice.leaveChannel();
                    resolve();
                }).on('error', () => {
                    reject();
                });
            });
        });
        return promise;
    };

    public static playFile(file: string, channel?: Discord.VoiceChannel): Promise<void> {
        const promise: Promise<void> = new Promise<void>((resolve, reject) => {
            if(!channel && !EpsilonVoice.info.channel && !EpsilonVoice.info.lastChannel) reject();
            EpsilonVoice.joinChannel(channel || EpsilonVoice.info.channel || EpsilonVoice.info.lastChannel!).then(() => {
                EpsilonVoice.info.connection!.playFile(file).on('end', () => {
                    if(EpsilonVoice.settings.leaveAfterFinishing) EpsilonVoice.leaveChannel();
                    resolve();
                }).on('error', () => reject());
            });
        });
        return promise;
    };
}

