import { EpsilonModule } from '../epsilonmodule';
import EpsilonResponse from '../epsilonresponse';
import { EpsilonMessageCommand } from '../epsiloncommand';
import { Message }  from 'discord.js';
import imagemagick from 'imagemagick';
import * as readline from 'readline';

class MemeOverlay {
    constructor(
    public x: number,
    public y: number,
    public width: number,
    public height: number,
    public layer: number
    ) {};
}

class MemeTemplate {
    constructor(
        public filename: string,
        public overlays: MemeOverlay[]
    ) {};
}

let templates: MemeTemplate[] = [];
let overlays: string[] = [];

function requestMemeCreation(message: Message): EpsilonResponse {
    return new EpsilonResponse('generowanie mema...', 0, createMeme().then(res => {
        return new EpsilonResponse('mem gotowy', 10, null, [], res);
    }));
}

function createMeme(): Promise<string> {
    const chosenTemplate = templates[Math.floor(Math.random() * templates.length)];
    const chosenOverlays: string[] = [];
    const alreadyUsed: number[] = [];

    for(let i = 0; i < chosenTemplate.overlays.length; i++) {
        let newlyChosen = Math.floor(Math.random() * overlays.length);
        while(alreadyUsed.indexOf(newlyChosen) != -1) newlyChosen = Math.floor(Math.random() * overlays.length);

        chosenOverlays.push(overlays[Math.floor(Math.random() * overlays.length)]);
    }

    const promise = new Promise<string>((resolve, reject) => {
        imagemagick.identify('./epsilon_modules/memegenerator/templates/' + chosenTemplate.filename, (err, features) => {
            if(err) return reject('wystąpił błąd generatora #1: ' + err);
            const query = [
                '-size', features.width+'x'+features.height, 'xc:none', '-fill', 'black'
            ];
            const under = [];
            const over = [];
            const background = ['./epsilon_modules/memegenerator/templates/' + chosenTemplate.filename,  '-geometry', '+0+0', '-composite']

            for(let i = 0; i < chosenTemplate.overlays.length; i++) {
                switch(chosenTemplate.overlays[i].layer) {
                    case -1:
                        under.push(
                            './epsilon_modules/memegenerator/overlays/' + chosenOverlays[i], '-geometry',  
                            chosenTemplate.overlays[i].width + 'x' + chosenTemplate.overlays[i].height + '!' + 
                            (chosenTemplate.overlays[i].x >= 0 ? '+' : '') + chosenTemplate.overlays[i].x +
                            (chosenTemplate.overlays[i].y >= 0 ? '+' : '') + chosenTemplate.overlays[i].y, '-composite'
                        );
                        break;

                    default:
                        over.push(
                            './epsilon_modules/memegenerator/overlays/' + chosenOverlays[i], '-geometry',
                            chosenTemplate.overlays[i].width + 'x' + chosenTemplate.overlays[i].height + '!' +
                            (chosenTemplate.overlays[i].x >= 0 ? '+' : '') + chosenTemplate.overlays[i].x +
                            (chosenTemplate.overlays[i].y >= 0 ? '+' : '') + chosenTemplate.overlays[i].y, '-composite'
                        );
                        break;
                }
            }

            const finishedQuery = [...query, ...under, ...background, ...over];
            imagemagick.convert([...query, ...under, ...background, ...over, './epsilon_temporary/lastmeme.png'], (err, stdout) => {
                if(err) return reject('wystąpił błąd generatora #2: ' + err);
                return resolve('./epsilon_temporary/lastmeme.png');
            });
        });
    });

    return promise;
};

export class MemeGenerator extends EpsilonModule {
    name = 'Meme Generator';
    messageCommands = [
        new EpsilonMessageCommand(new RegExp(/^;meme$/), requestMemeCreation)
    ];
    reactionCommands = [];

    getPresence(): { presence: string, priority: number } {
        return {
            presence: '',
            priority: 0
        };
    }

    init(): boolean {
        const lineReader = readline.createInterface({
            input: require('fs').createReadStream('./epsilon_modules/memegenerator/templates.txt')
        });

        lineReader.on('line', line => {
            const properties = line.split('|');
            if(properties.length < 2) return;
            const newTemplate: MemeTemplate = new MemeTemplate(properties[0], []);

            for(let i = 1; i < properties.length; i++) {
                const ovProps = properties[i].split(' ');
                if(ovProps.length != 5) continue;
                newTemplate.overlays.push(new MemeOverlay(Number(ovProps[0]), Number(ovProps[1]), Number(ovProps[2]), Number(ovProps[3]), Number(ovProps[4])));
            }

            templates.push(newTemplate);
        });

        lineReader.on('close', () => console.log(this.name + ': loaded', templates.length, 'templates.'));

        overlays = require('fs').readdirSync('./epsilon_modules/memegenerator/overlays');
        console.log(this.name + ': loaded', overlays.length, 'overlays.');

        return true;
    }
}
