import { EpsilonModule } from '../epsilonmodule';
import EpsilonResponse from '../epsilonresponse';
import { EpsilonMessageCommand, EpsilonReactionCommand } from '../epsiloncommand';
import { Message } from 'discord.js';
import * as math from 'mathjs';
import Configstore from 'configstore';

const config = new Configstore('epsilon-bot-rpg');
let users: RPGUser[] = [];

class RPGMacro {
    public constructor(public name: string, public value: string) {};
}

class RPGUser {
    public constructor(id: string) {this.id = id};
    public id: string = '0';
    public macros: RPGMacro[] = []; 

    public static addMacro(user: RPGUser, name: string, value: string): boolean {
        for(let i = 0; i < user.macros.length; i++) {
            if(user.macros[i].name == name) user.macros.splice(i, 1);
        }
        user.macros.push(new RPGMacro(name, value));
        return true;
    };

    public static removeMacro(user: RPGUser, name: string): boolean {
        for(let i = 0; i < user.macros.length; i++) {
            if(user.macros[i].name == name) {
                user.macros.splice(i, 1);
                return true;
            }
        }
        return false;
    };
}

function getUser(userID: string): RPGUser {
    for(let u of users) {
        if(u.id == userID) return u;
    }

    return users[users.push(new RPGUser(userID)) - 1];
};

function listMacros(message: Message): EpsilonResponse {
    let user = getUser(message.author.id);
    if(user.macros.length == 0) return new EpsilonResponse('nie masz żadnych makr', 10, null);

    let output = '';
    for(let i in user.macros) {
        output += '\n' + (Number(i) + 1) + '. `' + user.macros[i].name + '` => `' + user.macros[i].value + '`';
    }

    return new EpsilonResponse(output, 10, null);
};

function addMacro(message: Message): EpsilonResponse {
    let user = getUser(message.author.id);
    let split = message.content.split(/\s+/);
    let name = split[1];
    let value = (() => {
        let connected = '';
        for(let i = 2; i < split.length; i++)
            connected += split[i] + (i < split.length - 1 ? ' ' : '');
        return connected;
    })();

    let done = RPGUser.addMacro(user, name, value);
    if(done) config.set('users', users);

    return new EpsilonResponse(
        done ?
        'dodano makro o nazwie `' + name + '` i wartości `' + value + '`' : 
        'nie udało się dodać makra `' + name + '`',
        10, null
    );
};

function removeMacro(message: Message): EpsilonResponse {
    let user = getUser(message.author.id);
    let name = message.content.split(/\s+/)[1];

    let done = RPGUser.removeMacro(user, name);
    if(done) config.set('users', users);

    return new EpsilonResponse(
        done ?
        'usunięto makro `' + name + '`' :
        'brak makra o nazwie `' + name + '`',
        10, null
    );
};

function resolve(message: Message): EpsilonResponse {
    const pattern = /\d*k\d+/;
    let split = message.content.split(/\s+/);
    let expression = (() => {
        let exp = '';
        for(let i = 1; i < split.length; i++)
            exp += split[i] + (i == split.length - 1 ? '' : ' ');
        return exp;
    })();
    let multiplier = split[0].match(/\d+/) ? Number(split[0].match(/\d+/)![0]) : 1;
    let output = '';

    multiplier = Math.min(multiplier, 50);
    expression = replaceMacros(message.author.id, expression);
    output += '`' + expression +'`: ';

    for(let i = 0; i < multiplier; i++) {
        let tempExpression = expression;
        while(pattern.test(tempExpression)) {
            let kSplit = String(pattern.exec(tempExpression)).split('k');
            if(kSplit[0] === '') kSplit[0] = '1';
            if(Number(kSplit[0]) > 1000) {
                return new EpsilonResponse('za duża liczba', 10, null);
            };

            let temp = 0;
            for(let a = 0; a < Number(kSplit[0]); a++)
                temp += Math.floor(Math.random() * Number(kSplit[1])) + 1;

            tempExpression = tempExpression.replace(String(pattern.exec(tempExpression)), '(' + temp + ')');
        }

        try {
            output += (multiplier == 1 ? '`' : '\n' + (i+1) + '. `') + math.eval(tempExpression) + '`';
        }
        catch(err) {
            output += err.message;
        }
    }

    return new EpsilonResponse(output, 10, null);
};

function replaceMacros(userID: string, expression: string): string {
    let user = getUser(userID);
    if(!user) return expression;
    for(let m of user.macros) {
        expression = expression.replace(new RegExp('!'+m.name, 'g'), '('+m.value+')');
    }

    return expression;
}

export class RPGCommands extends EpsilonModule {
    name = 'RPG Commands';
    messageCommands = [
        new EpsilonMessageCommand(new RegExp(/^;km\?/), listMacros),
        new EpsilonMessageCommand(new RegExp(/^;km\+\s+\S+\s+.+/), addMacro),
        new EpsilonMessageCommand(new RegExp(/^;km-\s+\S+/), removeMacro),
        new EpsilonMessageCommand(new RegExp(/^;\d*k\s+.+/), resolve)
    ];
    reactionCommands = [];

    init(): boolean {
        users = config.get('users');
        if(users == undefined) users = [];
        return true;
    }

    getPresence(): { presence: string, priority: number } {
        return {
            presence: '',
            priority: 0
        };
    }
}
