import { EpsilonModule } from '../epsilonmodule';
import EpsilonResponse from '../epsilonresponse';
import { EpsilonMessageCommand, EpsilonReactionCommand } from '../epsiloncommand';
import { Message, MessageReaction, User, VoiceChannel } from 'discord.js';
import { EpsilonVoice } from '../epsilonvoice';
import Configstore from 'configstore';
import * as fs from 'fs';

const config = new Configstore('epsilon-countdown');
const directory = './countdown/';
let settings = {
    selectedVoicePack: 'default'
};
let channel: VoiceChannel | null = null;

function countdown(message: Message): EpsilonResponse {
    channel = message.member ? message.member.voiceChannel || channel : channel;
    EpsilonVoice.playFile(directory + settings.selectedVoicePack + '.ogg', channel || undefined)
        .then(() => {})
        .catch(() => message.reply('nie udało mi się :('));

    return new EpsilonResponse('już odliczam', 10, null, ['🔄']);
}

function changeVoicePack(message: Message): EpsilonResponse {
    const name = message.content.slice(3);

    if(!fs.existsSync(directory + name + '.ogg')) {
        return new EpsilonResponse('brak takiego głosu!', 10, null);
    }

    settings.selectedVoicePack = name;
    config.set('settings', settings);
    return new EpsilonResponse('głos odliczania zmieniony na `' + name + '`.', 10, null);
};

function listVoicePacks(message: Message): EpsilonResponse {
    let result = 'dostępne głosy:';
    fs.readdirSync(directory).forEach(file => result += '\n- `' + file + '`');
    return new EpsilonResponse(result, 100, null);
};

export class Countdown extends EpsilonModule {
    name = 'Countdown';
    messageCommands = [
        new EpsilonMessageCommand(new RegExp(/\bodlicz(|aj|anie)\b/), countdown),
        new EpsilonMessageCommand(new RegExp(/^;ol\b/), listVoicePacks),
        new EpsilonMessageCommand(new RegExp(/^;os\s+.+/), changeVoicePack)
    ];
    reactionCommands = [
        new EpsilonReactionCommand('🔄', countdown)
    ];

    getPresence(): { presence: string, priority: number } {
        return {
            presence: '',
            priority: 0
        };
    }

    init() : boolean {
        settings = config.get('settings');
        return true;
    }
};
