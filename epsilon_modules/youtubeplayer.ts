require('dotenv').config();
import { EpsilonModule } from '../epsilonmodule';
import EpsilonResponse from '../epsilonresponse';
import { EpsilonMessageCommand, EpsilonReactionCommand } from '../epsiloncommand';
import { Message, MessageReaction, User, VoiceChannel } from 'discord.js';
import { EpsilonVoice } from '../epsilonvoice';
import * as https from 'https';
import * as querystring from 'querystring';
import ytdl from 'ytdl-core';

let settings = {
    q: '',
    part: 'snippet',
    key: 'unknown',
    maxResults: 20,
    type: 'video'
};

let results: any = {};
let stream: any = null;
let playing: boolean = false;
let channel: VoiceChannel | null = null;
let lastMessage : Message | null = null;
let videoNumber: number = 0;
const urlPrefix = 'https://youtu.be/';

function playVideo(url: string): EpsilonResponse {
    stream = ytdl(url, { filter: 'audioonly' });
    playing = true;
    EpsilonVoice.playStream(stream, channel || undefined)
        .then(() => playing = false)
        .catch(() => playing = false);

    return new EpsilonResponse(url, 100, null, ['⏮','⏹','▶','⏭']);
}

function playCurrentVideo(): EpsilonResponse {
    if(!results) return new EpsilonResponse('', 0, null);
    return playVideo(urlPrefix+results.items[videoNumber].id.videoId);
}

function stopVideo(): EpsilonResponse {
    return new EpsilonResponse(EpsilonVoice.leaveChannel() ? 'wyłączam odtwarzanie' : 'nie ma czego wyłączać', 1, null);
}

function playNextVideo(): EpsilonResponse{
    if(!results || videoNumber >= results.items.length - 1) return new EpsilonResponse('', 0, null);
    videoNumber++;
    return playVideo(urlPrefix+results.items[videoNumber].id.videoId);
}

function playPrevVideo(): EpsilonResponse{
    if(!results || videoNumber < 1) return new EpsilonResponse('', 0, null);
    videoNumber--;
    return playVideo(urlPrefix+results.items[videoNumber].id.videoId);
}

function search(keywords: string): Promise<string> {
    settings.q = keywords;

    let options = {
        host: 'www.googleapis.com',
        path: '/youtube/v3/search?' + querystring.stringify(settings),
        method: 'GET'
        //port: 443
    };

    let promise = new Promise<string>((resolve, reject) => {
        let request = https.get(options, (res) => {
            let concat = '';
            let bodyChunks: any[] = [];
            res.on('data', (chunk) => {
                concat += chunk;
            }).on('end', () => {
                return resolve(JSON.parse(concat));
            });
        });

        request.on('error', (err) => {
            return reject(err);
        });
    });

    return promise;
}

function findAndPlay(message: Message): EpsilonResponse {
    channel = message.member ? message.member.voiceChannel || channel : channel;

    const query = message.content.slice(3);
    return new EpsilonResponse('szukam filmów..', 0, search(query).then((res) => {
        results = res;
        videoNumber = 0;
        return playVideo(urlPrefix+results.items[0].id.videoId);
    }));
}

export class YouTubePlayer extends EpsilonModule {
    name = 'YouTube Player';
    messageCommands = [
        new EpsilonMessageCommand(new RegExp(/^;y\s+.+/), findAndPlay),
        new EpsilonMessageCommand(new RegExp(/^;yx/), stopVideo)
    ];

    reactionCommands = [
        new EpsilonReactionCommand('▶', playCurrentVideo),
        new EpsilonReactionCommand('⏹', stopVideo),
        new EpsilonReactionCommand('⏮', playPrevVideo),
        new EpsilonReactionCommand('⏭', playNextVideo)
    ];

    init(): boolean {
        settings.key = process.env.YOUTUBE_KEY || settings.key;
        return settings.key != 'unknown';
    };

    getPresence(): { presence: string, priority: number} {
        if(playing) return {
            presence: results.items[videoNumber].snippet.title,
            priority: 75
        };

        return {
            presence: '',
            priority: 0
        };
    };
}
