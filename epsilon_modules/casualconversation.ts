import { EpsilonModule } from '../epsilonmodule';
import EpsilonResponse from '../epsilonresponse';
import { EpsilonMessageCommand } from '../epsiloncommand';
import { Message } from 'discord.js';
import * as readline from 'readline';

class ConversationPart {
    constructor(
        public requests: string,
        public responses: string[],
        public priority: number
    ) {};
}

let conversationParts: ConversationPart[] = [];

function randomString(array: string[]) {
    return array[Math.floor(Math.random() * array.length)];
}

function execute(message: Message): EpsilonResponse {
    let reply = '';
    for(let cpart of conversationParts) {
        if(message.content.match(new RegExp(cpart.requests)))
            reply += randomString(cpart.responses) + ', ';
    }

    return new EpsilonResponse(reply.slice(0, -2), 10, null);
}


export class CasualConversation extends EpsilonModule {
    
    name = 'Casual Conversation';
    messageCommands = [
        new EpsilonMessageCommand(new RegExp(/.+/), execute)
    ];
    reactionCommands = [];

    
    getPresence(): { presence: string, priority: number } {
        return {
            presence: '',
            priority: 0
        };
    }

    init(): boolean {
        let lineReader = readline.createInterface({
            input: require('fs').createReadStream('./epsilon_modules/casualconversation/database.txt')
        });

        let lineCount = 0;
        let requests: string, responses: string, priority: number;

        lineReader.on('line', (line) => {
            lineCount++;
            switch(lineCount) {
                case 1: requests = line; break;
                case 2: responses = line; break;
                case 3: priority = line;
                        conversationParts.push(new ConversationPart(
                            requests,
                            responses.split('|'),
                            priority
                        ));
                        break;
                case 4: lineCount = 0; break;
            }
        });

        lineReader.on('close', () => {
            conversationParts.sort((a, b) => { return a.priority < b.priority ? -1 : 1; });
            console.log(this.name + ': loaded', conversationParts.length, 'conversation parts.');
        });

        return true;
    };
}
