import env from 'dotenv';

import { EpsilonModule } from '../epsilonmodule';
import EpsilonResponse from '../epsilonresponse';
import { EpsilonMessageCommand, EpsilonReactionCommand } from '../epsiloncommand';
import { Message } from 'discord.js';
import Configstore from 'configstore';
import AES from 'crypto-js/aes';
import Socketio from 'socket.io';
import express from 'express';
import http from 'http';
import pug from 'pug';

env.config();

const config = new Configstore ('epsilon-standalone-server');
const app = express();
const server = new http.Server(app);
const io = Socketio(server);

app.use(express.static(__dirname + '/standaloneserver/public'));
app.set('views', __dirname + '/standaloneserver/views');
app.set('view engine', 'pug');

let users: StandaloneUser[] = [];

class StandaloneUser {
    public constructor(id: string) {this.id = id};
    public id: string = '';
    public token: string = '';
    public connected: boolean = false;
    public admin: boolean = false;

    public connect(): boolean {
        this.connected = true;
        return true;
    };

    public sendMessage(message: string): boolean {
        if(!this.connected) {
            if(!this.connect())
                return false;
        }

        const encrypted = AES.encrypt(message, this.token);
        return true;
    };
}

function getUser(userID: string): StandaloneUser {
    for(let u of users) {
        if(u.id == userID) return u;
    }

    return users[users.push(new StandaloneUser(userID)) - 1];
}

function sendGlobalMessage(message: Message): EpsilonResponse {
    io.emit('serverGlobalMessage', message.content, {for: 'everyone'});
    return new EpsilonResponse('', 10, null);
};

function sendIndividualMessage(message: Message): EpsilonResponse {
    const user = getUser(message.author.id);
    if(!user.admin) return new EpsilonResponse('nie masz praw do przesyłania komend do użytkowniów!', 10, null);

    const split = message.content.split(/\s+/);
    return new EpsilonResponse('ta funkcja nie jest jeszcze ukończona!', 10, null);
};

export class StandaloneServer extends EpsilonModule {
    name = 'Standalone Server';
    messageCommands = [
        new EpsilonMessageCommand(new RegExp(/^(?!;ss).*$/), sendGlobalMessage),
        //new EpsilonMessageCommand(new RegExp(/^;ss\+$/), addUser),
        //new EpsilonMessageCommand(new RegExp(/^;ss-/), removeUser),
        //new EpsilonMessageCommand(new RegExp(/^;ss\?/), getStatus),
        new EpsilonMessageCommand(new RegExp(/^;ss\s+/), sendIndividualMessage)
    ];
    reactionCommands = [];

    getPresence(): { presence: string, priority: number } {
        return {
            presence: '',
            priority: 0
        };
    }

    init(): boolean {
        users = config.get('users');
        if(users == undefined) users = [];

        io.on('connection', (socket) => {
            console.log('A new StandaloneServer user connected.');
        });

        app.get('/', (req, res) => {
            res.render('client.pug', {
                title: process.env.STANDALONE_SERVER_TITLE || process.env.BOT_NAME + ' Client' || 'Epsilon Client'
            });
        });

        server.listen(process.env.STANDALONE_SERVER_PORT || 3000); 

        return true;
    }
}

