import EpsilonResponse from './epsilonresponse';
import { EpsilonMessageCommand, EpsilonReactionCommand } from './epsiloncommand';
import * as Discord from 'discord.js';

export abstract class EpsilonModule {
    public init(discord?: Discord.Client): boolean {
        return true;
    };

    public abstract getPresence(): { presence: string, priority: number };
    
    public getMessageResponse(message: Discord.Message): EpsilonResponse {
        for(let mc of this.messageCommands) {
            if(message.content.match(mc.regexp)) return mc.func(message);
        }
        return new EpsilonResponse('', 0, null);
    };

    public getReactionResponse(reaction: string, message: Discord.Message, user: Discord.User): EpsilonResponse {
        for(let rc of this.reactionCommands) {
            if(rc.reaction == reaction) return rc.func(message, user);
        };
        return new EpsilonResponse('', 0, null);
    };

    public abstract name: string;
    public abstract messageCommands: EpsilonMessageCommand[];
    public abstract reactionCommands: EpsilonReactionCommand[];
}
