import env from 'dotenv';

import * as Discord from 'discord.js';
import fs from 'fs';

import { EpsilonModule } from './epsilonmodule';
import EpsilonResponse from './epsilonresponse';

env.config();
const client = new Discord.Client();

let botInfo = {
    name: 'EpsilonBot',
    ID: 'unknown',
    secret: 'unknown',
    wakewords: ['epsilon| eps | eps'],
    defaultResponse: 'I don\'t understand',
    noWakewordSeconds: 10,
    lastChannel: null,
    initialized: false
};
let modules: EpsilonModule[];
let lastResponseTime = 0;
let lastChannel: Discord.TextChannel | Discord.DMChannel | Discord.GroupDMChannel | null = null;

export function init(...modulesToLoad: EpsilonModule[]): boolean {
    botInfo.name = process.env.BOT_NAME || botInfo.name;
    botInfo.ID = process.env.BOT_ID || botInfo.ID;
    botInfo.secret = process.env.BOT_SECRET || botInfo.secret;
    botInfo.wakewords = (process.env.BOT_WAKEWORDS || botInfo.wakewords[0]).split('|');
    botInfo.defaultResponse = process.env.BOT_DEFAULT_RESPONSE || botInfo.defaultResponse;
    botInfo.noWakewordSeconds = Number(process.env.NO_WAKEWORD_SECONDS) || botInfo.noWakewordSeconds;
    modules = modulesToLoad;

    if(botInfo.ID == 'unknown' || botInfo.secret == 'unknown') return botInfo.initialized = false;

    for(let mod of modules) {
        if(mod.init())
            console.log('Module', mod.name, 'initialized.');
        else
            console.log('Initialization of module', mod.name, 'failed.');
    }

    client.on('ready', () => {
        console.log(botInfo.name, 'is active.');
        console.log('Wakewords: ', botInfo.wakewords);
    });

    client.on('message', respondToMessage);
    client.on('messageReactionAdd', respondToReaction);
    client.on('messageReactionRemove', respondToReaction);
    client.login(botInfo.secret).then(() => {
        setPresence();
    });

    return botInfo.initialized = true;
}

function handleResponse(response: EpsilonResponse, message: Discord.Message, user?: Discord.User) {
    if(response.text) {
        let newMessage = user ? message.channel.send('<@'+user.id+'>, ' + response.text) : message.reply(response.text);
        if(response.reactions)
            for(let r of response.reactions) {
                newMessage.then((n) => (n as Discord.Message).react(r));
            }
    }
    if(response.promise) response.promise.then(next => handleResponse(next, message, user)).catch(err => message.reply(err));
}

function setPresence() {
    if(!client.user) return;

    let current: {
        presence: string,
        priority: number
    } = {
        presence: '',
        priority: 0
    };

    for(let mod of modules) {
        let next = mod.getPresence();
        if(next.priority > current.priority) current = next;
    };

    if(!client.user.presence.game || client.user.presence.game.name != current.presence) 
        client.user.setPresence({
            status: 'online',
            afk: false,
            game: {
                name: current.presence
            },
        });

    setTimeout(setPresence, 2*1000);
}

function respondToReaction(reaction: Discord.MessageReaction, user: Discord.User) {
    if(user.id == botInfo.ID || reaction.message.author.id != botInfo.ID) return;
    for(let mod of modules) {
        handleResponse(mod.getReactionResponse(reaction.emoji.toString(), reaction.message, user), reaction.message, user);
    }
}

function respondToMessage(message: Discord.Message): void {
    if(!botInfo.initialized || message.author.id == botInfo.ID) return;
    if(!containsAny(message.content, ...botInfo.wakewords) 
        && message.content[0] != ';' 
        && (Date.now() / 1000) > lastResponseTime + botInfo.noWakewordSeconds
        && lastChannel != message.channel
        && (message.channel instanceof Discord.TextChannel))
        return;
    let reply: string = '';
    let responses: EpsilonResponse[] = [];
    let responseReactions: string[] = [];

    for(let mod of modules) {
        let newResponse = mod.getMessageResponse(message);
        responses.push(newResponse);
        if(newResponse.reactions) responseReactions = [...responseReactions, ...newResponse.reactions];
    }

    responses.sort((a, b) => {return a.priority < b.priority ? 1 : -1;});

    let attachment = null;
    for(let response of responses) {
        if(response.text) reply += response.text + ', ';
        if(response.promise) {
            response.promise.then((res) => {
                message.reply(res.text, res.attachment ? {files: [{attachment: res.attachment}]} : undefined).then((sent) => {
                    if(!res.reactions) return;
                    for(let reaction of res.reactions)
                        (sent as Discord.Message).react(reaction);
                });
            }).catch((err) => {
                if(err) message.reply(err);
            });
        }
        if(response.attachment && !attachment) attachment = response.attachment;
    }

    reply = reply.slice(0, -2) + '.';
    if(reply == '.') {
        if(!containsAny(message.content, ...botInfo.wakewords) && message.content[0] != ';') return;
        reply = botInfo.defaultResponse;
    }
    message.reply(reply, attachment ? {files: [{attachment: attachment}]} : undefined).then((sent) => {
        if(!responseReactions) return;
        for(let reaction of responseReactions)
            (sent as Discord.Message).react(reaction);
    }).catch((err) => { console.log(err); });
    
    lastResponseTime = Date.now() / 1000;
    lastChannel = message.channel;
}

function containsAny(str: string, ...substrings: string[]): boolean {
    for(let sub of substrings) {
        if(str.indexOf(sub) != -1) return true;
    }
    return false;
}
