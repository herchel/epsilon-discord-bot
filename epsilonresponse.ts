export default class EpsilonResponse {
    constructor(public text: string, public priority: number, public promise: Promise<EpsilonResponse> | null, public reactions?: string[], public attachment?: string) {};
}
