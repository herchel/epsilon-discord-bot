require('dotenv').config();
import * as Epsilon from "./epsilon";

// Modules
import { CasualConversation } from './epsilon_modules/casualconversation';
import { RPGCommands } from './epsilon_modules/rpgcommands';
import { YouTubePlayer } from './epsilon_modules/youtubeplayer'
import { StandaloneServer } from './epsilon_modules/standaloneserver';
import { MemeGenerator } from './epsilon_modules/memegenerator';

Epsilon.init(new CasualConversation, new RPGCommands, new YouTubePlayer, new StandaloneServer, new MemeGenerator);
