# Epsilon
Epsilon is a Discord bot written in TypeScript & Node.js. It was made for private use but I don't see a reason not to share it publicly.

## Bugs
* Many commands return messages written in Polish! As I said, Epsilon was made for private use. English language is on my to-do list, though.

## Built-in modules
* Casual Conversation - a simple "conversation" module (responding to predefined keywords)
    - Keywords can use RegExp
    - The database must be stored in `epsilon_modules/casualconversation/database.txt`
    - I haven't uploaded an example database yet, but I'll do it soon
* RPG Commands - lets you do virtual dice rolls with a simple syntax. Includes macros!
    - `;k 2k6` rolls two d6 dice
    - `;k k100*4+10` rolls one d100 die, multiplies the result by 4 and adds 10 to it
    - `;10k k6` does 10 **separate** d6 dice rolls and displays results in a column
    - `;k 2*2+6` no dice here but it is possible as well
    - `;km+ macroname` adds a new macro that can be used by writing `!macroname`
    - `;km- macroname` removes the macro called `macroname`
    - `;km?` lists all of your macros (they're separate for each user)
    - `;k k100+!macroname` rolls one d100 die and adds the value of macro `macroname` to it
* YouTube Player - lets you search & play audio from YouTube
    - `;y klocuch recenzje` searches for videos with the phrase `klocuch recenzje` and plays the first one
    - `;yx` stops the currently playing video
    - Buttons are displayed along with search results that let you control the playback (Play/Stop/Next video/Prev video)

## Casual Conversation - how to create a database?
Create a file with the following path - `epsilon_modules/casualconversation/database.txt` and structurize it like this:
```
<RegExp for the first response pack>
<Possible responses separated with |>
<A number representing the priority of this response pack>
<A blank line>
<RegExp for the second response pack>
<Possible responses separated with |>
<A number representing the priority of this response pack>
<A blank line>
etc...
```
for example:
```
he(n|l)lo|hi|hey
Hello!|Hi there!|Heeeellloooo!
95

what's up|wa(d+)up|how's (it|everything) going
it's okay|it's alright|I've been better
80
```
## Requirements
* A computer with Node.js and some package manager (I'm using npm),
* Some computer knowledge,
* Discord API keys,
* YouTube API keys (optional, for YouTube playback functionality)

## Installation
1. Clone this repository
2. Create an .env file in the directory with all of the files with the following contents:
```
BOT_ID=your Discord API bot ID
BOT_SECRET=your Discord API bot secret
BOT_WAKEWORDS=wakewords seperated with | (example: BOT_WAKEWORDS=epsilon|mybot|botbot)
BOT_DEFAULT_RESPONSE=What should the bot respond with in case of an unrecognized command
BOT_NAME=The bot's name
YOUTUBE_KEY=YouTube API key
```
3. Create an empty file with the following path: `epsilon_modules/casualConversation/database.txt` (or just create a real database there, but an empty file will do)
4. Run `npm install` to install the required dependencies
5. Run `npm start` or something to get the thing running.

## In the future
Here's stuff I want to include in the future iterations of Epsilon:
* SoundCloud playback with the SoundCloud API,
* Countdown function