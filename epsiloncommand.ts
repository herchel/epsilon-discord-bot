import EpsilonResponse from './epsilonresponse';
import { Message, User } from 'discord.js';

export class EpsilonMessageCommand {
    public constructor(
        public regexp: RegExp,
        public func: (message: Message) => EpsilonResponse
    ) {};
}

export class EpsilonReactionCommand {
    public constructor(
        public reaction: string,
        public func: (message: Message, user: User) => EpsilonResponse
    ) {};
}
